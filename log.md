## 2.2
Хост система - Void Linux.
```
$ uname -srvo
Linux 5.8.18_1 #1 SMP Sun Nov 1 14:25:13 UTC 2020 GNU/Linux
```

Для установки системы необходимы некоторые пакеты:
```bash
$ for p in bash binutils bison bzip2 coreutils diffutils findutils gawk gcc glibc grep gzip linux m4 make patch perl python sed tar texinfo xz
do
    xbps-query -p pkgver $p || echo -e "\e[31mNot found: $p\e[0m"
done
```
```
bash-5.0.018_1
binutils-2.34_1
bison-3.7.4_1
bzip2-1.0.8_1
coreutils-8.32_4
diffutils-3.7_1
findutils-4.7.0_1
gawk-5.1.0_1
gcc-9.3.0_8
glibc-2.30_1
grep-3.6_1
gzip-1.10_1
linux-5.8_2
m4-1.4.18_2
make-4.3_3
patch-2.7.6_4
perl-5.32.0_1
python-2.7.18_2
sed-4.8_1
tar-1.32_3
texinfo-6.7_1
xz-5.2.5_1
```

Все необходимые пакеты уже установлены.
Чтобы установить отсутствующие пакеты, можно заменить `echo...` на `xbps-install $p`.

Потенциальные проблемы с версиями:
- bash 3.2 -> 5.0
- bison 2.7 -> 3.7
- coreutils 6.9 -> 8.32
- diffutils 2.8 -> 3.7
- gawk 4.0 -> 5.1
- gcc 6.2 -> 9.3
- grep 2.5 -> 3.6
- **linux 3.2 -> 5.8**
- **python 3.4 -> 2.7** - Пакет называется по-другому: `xbps-query -p pkgver python3` -> `python3-3.9.0_1`
- texinfo 4.7 -> 6.7

Проверка сслыок:
```
/bin/sh -> dash
/usr/bin/yacc -> bison-yacc
/usr/bin/awk -> gawk
```

`/bin/sh` должна указывать на `bash`, а указывает на `dash`.
Исправляется с помощью `xbps-alternatives`:
```
$ sudo xbps-alternatives -s bash
Removing 'sh' alternatives group symlink: sh
Removing 'sh' alternatives group symlink: sh.1
bash-5.0.018_1: applying 'sh' alternatives group
Creating 'sh' alternatives group symlink: sh -> /usr/bin/bash
Creating 'sh' alternatives group symlink: sh.1 -> /usr/share/man/man1/bash.1
```

## 2.4
Есть три утилиты для настройки разделов диска - fdisk, cfdisk, sfdisk.
1. fdisk - старый, есть почти на любой unix системе
2. cfdisk - использует подобие графического интерфейса
3. sfdisk - удобно сохранить текущую настройку диска, править и/или загрузить обратно

Я буду использовать cfdisk, так как им удобнее всего пользоваться при ручной настройке.

Диск до добавления раздела lfs:

![Скриншот cfdisk](2.4_cfdisk1.png)

Диск после добавления раздела lfs:

![Скриншот cfdisk](2.4_cfdisk2.png)

Таким образом, раздел LFS - `/dev/sda7`, раздел swap - `/dev/sda6`

## 2.5
Необходимо отформатировать только раздел LFS, swap раздел уже настроен и используется.
```
$ sudo mkfs.ext4 /dev/sda7
mke2fs 1.45.6 (20-Mar-2020)
/dev/sda7 contains a ext4 file system
	last mounted on /sysroot on Wed Oct 28 15:53:29 2020
Proceed anyway? (y,N) y
Creating filesystem with 7864320 4k blocks and 1966080 inodes
Filesystem UUID: e776cbdf-3d83-4337-95c7-21d9991db585
Superblock backups stored on blocks: 
	32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632, 2654208, 
	4096000

Allocating group tables: done                            
Writing inode tables: done                            
Creating journal (32768 blocks): done
Writing superblocks and filesystem accounting information: done
```

## 2.6, 2.7
Я буду использовать `/lfs` в качестве точки монтирования. Для этого нужно создать саму папку:
```bash
$ sudo mkdir $LFS
$ export LFS=/lfs
```

Я не хочу править `.bashrc` и `fstab`, поэтому я создам скрипт, который будет создавать переменную `$LFS` и монтировать раздел:
```bash
export LFS=/lfs
sudo mount /dev/sda7 $LFS
```

Так как скрипт задает переменную, его надо вызывать с помощью `source lfs.sh`.

## 3.1
В книге большинство сслыок на скачивание используют протокол http, а не https. Чтобы не править ссылки вручную, можно использовать такую команду:
```bash
function wg {
  echo "$@" | sed 's/\<http:\/\//https:\/\//g' | xargs wget;
}
```

Также чтобы не надо было каждый раз писать "wg":
```bash
while true; do printf '> ' && read && wg "$REPLY"; done
```

В файле `packages.md` содержится краткое описание каждого пакета в системе с ссылками на интересные материалы и примеры использования.


## Общее по главам 4, 5, 6
Архивы поместил в `$LFS/sources/tarballs`, patch-файлы в `$LFS/sources/patches`.

Убрал права на запись для всех архивов, чтобы случайно не удалить - `chmod a-w *` в папке с архивами.

Довольно часто использую сочетание клавиш `Ctrl+X Ctrl+E` в bash - открывает текущую команду в текстовом редакторе и выполняет ее (их) при выходе из него. Удобно для редактирования многострочных команд. Текстовый редактор можно задать с помощью переменной `$VISUAL`, например `export VISUAL=nano`.

При удалении приходится использовать `rm -rf`, всегда ставлю '/' в конце папки чтобы случайно не удалить архив.

`alias c='rm -rf $(find -mindepth 1 -maxdepth 1 -type d)'` - удаляет все папки в текущей.

В `make check` "XFAIL" означает "eXpected fail" - разработчики знают о том, что эти тесты не проходят, и это не означает, что с текущей системой что-то не так.

## 4.4
Добавил свой `.bashrc` в дополнение к тому, что есть (только включение цвета для команд как `ls`, `grep`, и цветной `$PS1`)
## 4.5
`export MAKEFLAGS='-j4'` идет в мой `.bashrc` и в `.bashrc` для lfs.
## 5.2
SBU - 1m37s.
Странные показатели time:
```
real	1m37.583s
user	3m53.555s
sys		0m47.023s
```

Обычно real = user + sys. Наверно из-за того, что использовалось несколько потоков (`make -j4`): user/4 + sys = 1m45s
## 5.3
`sed -e '/m64=/s/lib64/lib/' -i.orig gcc/config/i386/t-linux64` - 
заменить `lib64` на `lib` в строке, где есть `m64=` в файле `gcc/.../t-linux64`, сохранив оригинал в `gcc/.../t-linux64.orig`.

Использую `--with-glibc-version=2.30` (вместо 2.11).
## 5.4
`find usr/include -name '.*' -delete` - удалить все скрытые файлы (не связано с расширением).
## 5.5
`ln -sfv ../lib/ld-linux-x86-64.so.2 $LFS/lib64` - создает файл `$LFS/lib64/ld-linux-x86-64.so.2` - ссылка на `../lib/ld-linux-x86-64.so.2`. В текущей папке это не имеет смысла, но так как ссылка существует в `$LFS/lib64`, то ссылка будет вести на `$LFS/lib64/../lib/ld-linux*`.

Использую `--enable-kernel=5.9` (вместо 3.2).

## 6.3
В файле присутствует такая строка: `for ac_prog in mawk gawk nawk awk`. Sed убирает из нее `mawk`.

`make -C` запускает `cd` перед вызовом `make`. Соотв.
`make -C progs tic` запускает `make` для файла `progs/Makefile` с аргументом `tic`.

В `ln -sfv ../../lib/$(readlink $LFS/usr/lib/libncursesw.so) $LFS/usr/lib/libncursesw` чуть не изменил `../../` на `$LFS/`. Опять, ссылка находится не в текущей папке, а в папке `$LFS/usr/lib/libncursesw`, соотв. `../../` на самом деле указывает на `$LFS/usr/lib/../../`.

## 6.7
Интересное предупреждение при `make install`:
```
libtool: warning: 'libmagic.la' has not been installed in '/usr/lib'
```

## 6.8
В команде `sed -i 's|find:=${BINDIR}|find:=/bin|' $LFS/usr/bin/updatedb` не подставляется `${BINDIR}`, так как в одинарных кавычках.

## 7.3
### 7.3.1
Не очень понятно, зачеем нужен этот шаг, если копируется `/dev/`, в котором `console` и `null` уже есть. К тому же файлы в любом случае исчезнут при вызове следующей команды `mount`. Попробую его пропустить.	

Монтируется `/dev/pts`, но не монтируется `/dev/shm`. Странно.

### 7.3.2
Можно было использовать `mount --rbind` чтобы смонтировать все дерево `/dev` одной командой.

## 7.4
Использую такой скрипт:

```bash
#!/bin/sh
export LFS='/lfs'
mount -v --bind /dev $LFS/dev &&
mount -v --bind /dev/pts $LFS/dev/pts &&
mount -vt proc proc $LFS/proc &&
mount -vt sysfs sysfs $LFS/sys &&
mount -vt tmpfs tmpfs $LFS/run &&
chroot "$LFS" /usr/bin/env -i \
	HOME=/root TERM="$TERM" \
	PS1='\W\e[033m\$\e[0m ' \
	MAKEFLAGS='-j4' \
	PATH=/bin:/usr/bin:/sbin:/usr/sbin \
	/bin/bash --login +h &&
umount $LFS/dev/pts &&
umount $LFS/dev &&
umount $LFS/proc &&
umount $LFS/sys &&
umount $LFS/run
```

## 7.5
Команда `install` отличается от команды `cp` только тем, что `install` дополнительно позволяет задать настройки файла
(`install -m 0750 foo bar` эквивалентно
`cp foo bar && chmod 0750 bar`).

## 7.7
`CXXFLAGS="-g -O2 -D_GNU_SOURCE"` - флаг `-g` включает создание отладочных символов (debugging symbols). Они не обязательны и [могут занимать много места](https://en.wikipedia.org/wiki/Debug_symbol#cite_ref-TechNet_Symbols_2-0).

## 8.14
`make` не находит библиотеку ncursesw, видимо 6.2 не было установлено. Компилирую ncurses заново без `--host=$LFS_TGT` и `DESTDIR=$LFS`, заменяя везде `$LFS`. После компияции ncurses readline успешно линкуется.

## 8.28
```bash
for lib in ncurses form panel menu ; do
    rm -vf                    /usr/lib/lib${lib}.so
    echo "INPUT(-l${lib}w)" > /usr/lib/lib${lib}.so
    ln -sfv ${lib}w.pc        /usr/lib/pkgconfig/${lib}.pc
done
```

Файл `/usr/lib/libncurses.so` уже есть с нужным (`INPUT(-lncursesw)`) содержимым. Но файлов `libform.so`, `libpanel.so` и `libmenu.so` нет.

## 8.30
`Finally, move the killall and fuser programs to the location specified by the FHS:` - в моей хост системе программы `killall` по умолчанию нет вообще (как и пакета psmisc).

Не нашел в FHS ничего про библиотеки в `/lib` и `/usr/lib` - единственное, что в них должно быть - `/lib./libc.so.*` и `/lib/ld*`. [FHS /lib](https://refspecs.linuxfoundation.org/FHS_3.0/fhs/ch03s09.html) [FHS /usr/lib](https://refspecs.linuxfoundation.org/FHS_3.0/fhs/ch04s06.html)

## 8.40
Если убрать `-des`, то скрипт будет спрашивать в интерактивном режиме, какие настройки использовать - например, можно задать флаг `-O3` при компиляциии вместо стандартного `-O2`. Скрипт поддерживает переменные `$1` `$2`, ... `$*`, подстановку команд и пр.

## 8.60
Один из 22 тестов не проходит успешно. Тест называется "zmore", скорее всего тестирует часть пакета - команду `zmore`, которая не работает, так как не установлена программа `more` (более старая версия `less`), поэтому его результат можно не учитывать.

## 8.69
### 8.69.1
Пакет не был скачан заранее из-за проблем с сервером linuxfromscratch.org, поэтому буду использовать последнюю версию vim - 8.2.2327 на текущий момент.

Чтобы не открывать файл `vim-test.log`, можно воспользоваться командой `grep -c 'ALL DONE' vim-test.log` - `-c` показывает вместо значения найденых строк их количество.

## 8.69.2
С версии vim 7.4.2111 существует файл `/usr/share/vim/vim*/defaults.vim`, в котором есть более полная альтернатива предложенным настройкам. Так выглядит только что установленый vim с настройками по умолчанию:

![vim screenshot](8.69.2_vimdefaults.png)

## 10.3
Использовал `make nconfig` вместо `make menuconfig` - более красивый интерфейс (ncurses).

Интересная опция - `make randconfig` - случайные значения для всех настроек.

Настроил ядро "по вкусу":
- Сжатие ядра с помощью ZSTD, а не gzip (сжатие хуже но быстрее) - места на диске хватит, а запуск системы чем быстрее - тем лучше
- Preemption Model - позволяет пожертвовать вычислительной мощностью ради улучшения отзывчивости системы, или наоборот. Использовал значение по умолчанию - "Desktop"
- Checkpoint/restore support - позволяет делать снимки процессов и позже их восстанавливать - давно хотел проверить, как это работает
- Page allocator randomization - написано, что улучшает работу кэша, почему бы не попробовать включить
- Отключил некоторые функции управления питанием для ноутбуков
- В Networking support можно включить поддержку Bluetooth и NFC, но мне это не требуется
- В File systems включил exFAT, NTFS, NTFS write support, Overlayfs, SquashFS с ZLIB и ZSTD. Из интересных еще есть F2FS - система, оптимизированная для SSD дисков и флеш-памяти. Также включил FUSE - поддержку создания своих файловых систем, таких как:
  - sshfs чтобы подключать диски по ssh
  - filterfs чтобы прятать файлы
  - clamfs чтобы прогонять файлы через антивирус при чтении
  - avfs создает для каждого архива по папке с содержимым
  - scriptfs заменяет все скрипты на результаты их выполнения
  - gitfs автоматом записывает все изменения файлов как коммиты
- Прочие настройки, выбранные командой `make defconfig`

## 10.4
Устанавливать GRUB не обязательно - можно использовать GRUB на хост-системе. Команда `update-grub` находит новую систему и добавляет ее в список доступных при загрузке (диск с новой системой должен быть подключен во время выполнения команды).
На всякий случай проверю работоспособность системы до того, как устанавливать GRUB.

Система запустилась успешно, нужно было настроить интернет:
```bash
ip address add 192.168.1.17/24 dev enp4s0
ip link set enp4s0 up
ip route add default via 192.168.1.17 dev enp4s0
```

Также, судя по тому, что разрешение экрана было гораздо меньше чем обычно, я забыл установить драйверы для видеокарты (модуль ядра, `amdgpu` для видеокарт AMD).

В меню GRUB новая система называется "Unknown linux distribution". ~~Чтобы это исправить, можно создать файл /etc/os-release с содержимым:~~ Это исправляется в главе 11.1.
```bash
NAME="LFS"
ID="LFS"
DISTRIB_ID="LFS"
PRETTY_NAME="LFS"
```

## 11.1
Тут находится полее полная версия файла `/etc/os-release`.

`VERSION_CODENAME` и `DISTRIB_CODENAME` - обычно это название версии, например для Debian 10 название версии - "buster".

## Исправление ошибок
### Драйверы
- Добавил amdgpu как модуль ядра. `make` занял 3 минуты.
- Оказалось, что для моей видеокарты нужен `radeon` а не `amdgpu`. С включенным `radeon` (и как часть ядра и как модуль) экран (не система) зависает при запуске. В хост-системе используется `radeon`, как модуль.
[При этом используется](https://github.com/void-linux/void-packages/blob/master/srcpkgs/xf86-video-ati/template)
исходный код с [сайта Xorg](https://www.x.org/releases/individual/driver/xf86-video-ati-19.1.0.tar.bz2).
Вероятно я неправильно компилировал, или не задал какие-то настройки. 
Также в хост системе в grub используется команды
`load_video` и `set gfxpayload=keep` при запуске системы, возможно их и не хватает.
В любом случае установка драйверов для видеокарты скорее всего рассмотрена в BLFS, а в текущей системе нет графической оболочки, поэтому поддержка видеокарты не обязательна.

### Сеть
- Настроил статический ip как сказано в 9.5.1.

## Использование диска
![Диаграмма использования диска](12_disk-usage.png)

В файле `du.txt` находится отсортированный по размеру список всех файлов и папок в получившейся систем (вывод команды `du -h --exclude 'sources/*' | sort -rh`).

Итоговая система занимает 4.7 GB. Большая часть - GCC:
- /usr/libexec/gcc/x86_64-lfs-linux-gnu - 0.8 GB
- /usr/libexec/gcc/x86_64-pc-linux-gnu - 0.8 GB
- /tools - 1.3 GB
- /usr/bin/lto-dump - 0.25 GB

Вероятно, я ошибся при установке GCC, так как на хост-системе:
- /usr/libexec/gcc/x86_64-unknown-linux-gnu - 129 MB
- /usr/libexec/gcc/x86_64-pc-linux-gnu - отсутствует
- /tools - отсутствует
- /usr/bin/lto-dump - отсутствует

Конечно, на хост системе скорее всего нет отладочных символов, но это не объясняет наличие папок `/tools` и `/usr/libexec/gcc/x86_64-pc-linux-gnu`.
