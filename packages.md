### Acl
Напоминает SELinux. Позволяет точнее настроить права доступа к файлу, не изменяя обладателя и группу файла. Можно задать права для конкретного пользователя на конкретный файл.
[Подробнее](https://www.redhat.com/sysadmin/linux-access-control-lists)
### Attr
Работа с метаданными файлов в XFS, такими как превью изображения, кодировка и т.д. Работает только на XFS.
[Man page](https://linux.die.net/man/1/attr)
### Autoconf
Настраивает проект под используемую систему перед компиляцией.
Нужен для automake.
[autoconf и automake](https://unix.stackexchange.com/a/210)
### Automake
Создает Makefile перед компиляцией с такими командами как `make clean`, `make install`.
Скорее всего будет нужен при сборке системы.
[autoconf и automake](https://unix.stackexchange.com/a/210)
### Bash
...
### Bc
Калькулятор чисел любых размеров/точности.
Например: `echo 2^2048 | bc`
### Binutils
Ассемблер, линкер и утилиты для компиляции. Необходимо для gcc.
### Bison
Парсер текста, языка программирования, формата файла и т.д. Используется вместе с flex.
[Flex/Bison tutorial](https://aquamentus.com/flex_bison.html)
### Bzip2
Работа с архивами .bz
### Check
"Unit testing framework for C".
[Official tutorial](https://libcheck.github.io/check/doc/check_html/check_3.html)
### Coreutils
Основные команды, такие как `ls`, `echo`, `df`, `sort` и т.д. Обязательно.
### DejaGNU
Библиотека для тестирования программ.
### Diffutils
Команды для сравнения файлов и папок:
- `diff --color file1 file2`
- `diff --color ~/ /etc/skel/`
### E2fsprogs
Команды для работы с ext4 - mke2fs, tune2fs, resize2fs, e2fsck...
Среди них нет команды вроде `mount.e2fs`, так что вероятно этот пакет не обязателен для функционирования системы.
### Elfutils
ELF - формат исполняемых файлов и библиотек, пакет предоставляет утилиты для работы с ними.
[Список команд с описанием](https://sourceware.org/elfutils/)
### Eudev
Версия udev от Gentoo с улучшенной совместимостью со старыми ядрами и не systemd init-системами. udev - программа, которая создает файлы устройств, такие как `/dev/sda`, `/dev/sda1` и т.д.
### Expat
Парсер XML (используется в CMake, gdb, git и т.д)
### Expect
Утилита позволяет писать или создавать автоматически скрипты чтобы частично или полностью автоматизировать ввод данных в интерактивных приложениях
[Tutorial](https://www.geeksforgeeks.org/expect-command-in-linux-with-examples/)
### File
Определяет тип файла - `file /etc/passwd`
### Findutils
Предоставляет команды `find`, `locate`, `xargs`. Скорее всего обязательно.
### Flex
Парсер текста, языка программирования, формата файла и т.д. Может использоваться вместе с bison или отдельно.
[Flex/Bison tutorial](https://aquamentus.com/flex_bison.html)
### Gawk
Небольшой язык программирования для работы с данными в текстовом формате - разбивает данные на строки и ячейки. Например, вывести логины и имена всех не системных пользователей: `awk -F: '$3 >= 1000 { print $1, $5 }' /etc/passwd`.
[Learn awk](https://learnbyexample.github.io/learn_gnuawk/)
### GCC
Компиляторы C, C++, стандартные библиотеки и т.д.
### GDBM
Библиотека для создания баз данных типа ключ-значение. [Документация](https://www.gnu.org.ua/software/gdbm/manual/Intro.html)
### Gettext
Система, которая облегчает перевод программ и скриптов. [Tutorial](https://www.labri.fr/perso/fleury/posts/programming/a-quick-gettext-tutorial.html)
### Glibc
Реализация стандартной библиотеки C (`open`, `malloc`, `printf` и т.д). Обязательно.
### GMP
Библиотека для работы с большими числами, в основном для криптографии.
### Gperf
Генерирует код на C - хеш-таблицу и хеш-функцию без коллизий. [Пример использования](https://developer.ibm.com/technologies/linux/tutorials/l-gperf/)
### Grep
Ищет строки в файлах по куску текста - `grep --color=auto 'root' /etc/passwd`
### Groff
Форматирует текст с макросами, используется в основном для `man`, хотя может экспортировать в html, pdf, и т.д.
### Grub
Распространенный загрузчик системы (bootloader).
### Gzip
Работа с архивами .gz
### Iana-Etc
Файлы `/etc/services` и `/etc/protocols` - официальные номера/названия протоколов и известных портов (http - 80).
### Inetutils
Команды `ping`, `hostname`, `traceroute`. Клиент/сервер для ftp, telnet и др. [Документация/Список команд](https://www.gnu.org/software/inetutils/manual/html_node/index.html)
### Intltool
Позволяет объединить перевод нескольких файлов одной программы в один файл. Работает с переводом gettext.
### IPRoute2
Команды `ip addr`, `ip link`, `ip route` и др. Вместо `ifconfig`.
### Kbd
Шрифты и настройки клавиатуры для tty. [Arch wiki](https://wiki.archlinux.org/index.php/Linux_console/Keyboard_configuration)
### Kmod
Команды для манипуляций модулями ядра `lsmod`, `modprobe`, `insmod`.
### Less
Позволяет просматривать файлы, которые не помещаются на экран, разбивая их по страницам - `less /bin/autoconf` - <kbd>h</kbd> - help, <kbd>q</kbd> - quit.
### LFS-Bootscripts
Файлы для init-системы:
- Множество файлов в `/etc/rc.d/init.d/`
- `ifdown`, `ifup` в `/sbin/`
- `ipv4-static-route`, `ipv4-static`, `init-functions` в `/services/`
### Libcap
Позволяет более точно настраивать разрешения программы, чем root/не root. Например, `ping` запускается не от имени `root`, но может посылать ICMP пакеты (что обычно может делать только привилегированный пользователь) благодаря разрешению "CAP_NET_RAW" - `getcap /bin/ping`. [Подробнее](https://www.howtoforge.com/how-to-manage-linux-file-capabilities/)
### Libffi
Библиотека для использования в коде на одном языке функций написанных на другом языке. [Официальный сайт](https://sourceware.org/libffi/)
### Libpipeline
Библиотека на C для более удобного вызова цепочки процессов, такой как `cat f.txt | sort | nl | grep foo`. [Официальный сайт](http://libpipeline.nongnu.org/)
### Libtool
Библиотека, которая упрощает создание кроссплатформенных (только UNIX-подобные системы) статических библиотек.
### Linux
Ядро системы. Обязательно. На 01.12.2020 последняя стабильная версия ([Официальный сайт](https://www.kernel.org)) - 5.9.11, использую ее. [Linux kernel vs Linux system](https://unix.stackexchange.com/questions/94402/why-do-people-call-linux-a-kernel-rather-than-an-os)
### M4
Макропроцессор, нужен для Autoconf.
### Make
Программа, которая облегчает компиляцию проектов, состоящих из более чем одного файла. [Tutorial](https://makefiletutorial.com/)
### Man-db
Предоставляет команду `man` - документация по использованию команд, системных файлов, функций и др. - `man ls`
### Man-pages
Документация в `man` для основных команд, системных вызовов, функций стандартной библиотеки C, файловых систем, назначение некоторых системных файлов и папок и др. - `man ls`
### Meson
Система сборки - значительно облегчает создание фалов для make / ninja.
### MPC
Библиотека C для работы с большими комплексными числами.
### MPFR
Библиотека C для работы с числами с плавающей запятой с правильным округлением.
### Ncurses
Библиотека для создания подобия графического интерфейса в терминале. Пример программы на ncurses - `sudo cfdisk`
### Ninja
Система сборки. Более быстрый аналог make.
### OpenSSL
Библиотека для шифрования и SSL/TLS (https, ssh).
### Patch
Команда `patch`, позволяет восстановить из оригинального файла и файла с изменениями, полученного командой `diff`, измененный файл. Так можно сохранять несколько версий файла (папки с файлами / проекта) не в виде копий, а в виде небольшого файла с изменениями. Таким образом можно экономить место на диске или распространять набор поправок/улучшений для проекта, даже если проект находится в активной разработке.
### Perl
Высокоуровневый язык программирования. Используется для написания скриптов, обработки текста, написания бэкенда (Yahoo) и т.д.
### Pkg-config
Ищет расположение библиотек в системе, позволяя программисту использовать одну и ту же команду для компиляции на системах с библиотеками в разных местах. [Пример](https://en.wikipedia.org/wiki/Pkg-config#Synopsis)
### Procps
Команды, облегчающие использование `/proc`: `free`, `kill`, `ps`, `sysctl`, `top`, `uptime`, `watch` и др. Пример - `uptime` [Список команд](https://gitlab.com/procps-ng/procps/-/blob/master/README.md)
### Psmisc
Доп. программы к procps - `killall`, `fuser` и др. [Список команд](https://gitlab.com/psmisc/psmisc/-/blob/master/README.md)
### Python
...
### Python Documentation
...
### Readline
Библиотека для включения горячих клавиш в интерактивных приложениях, как в bash. Например, обычный `read` не поддерживает использование стрелок на клавиатуре - `read`.
### Sed
Небольшой язык програмирования для автоматизации редактирования текста - `sed 's/root/not_root/g' /etc/passwd`
### Shadow
Утилиты для работы `/etc/shadow` - файла с зашифрованными паролями пользователей.
### Sysklogd
Программы обеспечивает ведение системных логов и логов ядра
### Sysvinit
Система инициализации. Первый процесс в системе + контролирует работу сервисов.
### Tar
Работа с архивами .tar
### Tcl
Язык программирования "tcl" (и расширение "tk" для создания графического интерфейса).
### Tcl Documentation
Документация для tcl.
### Texinfo
Система для создания документов, пригодных для экспорта в html, pdf и пр. Альтернатива LaTeX. [Пример документа](https://en.wikipedia.org/wiki/Texinfo#Texinfo_source_file)
### Tzdata
Информация о временных поясях. Пример - `ls /usr/share/zoneinfo/Europe/`
### Udev-lfs
Дополнительные правила для eudev.
### Util-linux
Утилиты, такие как `cfdisk`, `chsh`, `dmesg`, `kill`, `mkswap`, `more`, `mount`, `reboot`, `sfdisk`, `shutdown` и др. [Полный список](http://freshmeat.sourceforge.net/projects/util-linux)
### Vim
Продвинутый текстовый редактор. Обучение основам vim - команда `vimtutor`. [Основные команды](https://devhints.io/vim)
### XML::Parser
Модуль для Perl, позволяющий использовать expat.
### Xz Utils
Работа с архивами .xz
### Zlib
Самая популярная библиотека для сжатия данных. Используется в ядре linux, git, OpenSSL и др. [Некоторые приложения, использующие zlib](https://zlib.net/apps.html)
### Zstd
Библиотека для сжатия данных от Facebook, быстрее чем zlib. [Официальный сайт](http://www.zstd.net/)
